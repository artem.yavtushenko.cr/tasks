from array import *
import random

# Массив для првоерки работоспособности алгоритма
# my_array = array('i', [10, 8, 10, 10, 12, 10])

# Создаем массив для записи 1000 рандомных чисел в диапазоне от -1000 до 1000
my_array = array('i')
# Создаем массив для записи чисел, которые удовлетворяют критериям поиска
my_array2 = array('f')

# Заполняем первый массив случайными числами
for i in range(0, 1000):
    x = random.randint(-1000, 1000)
    my_array.append(x)

# Для проверки
# print(my_array)
# print(sum(my_array))
# print(len(my_array))

# Вычисляем среднее арифметическое по массиву:
average_number = sum(my_array) / len(my_array)
# Вычисляем число, которое на 20 процентов больше, чем среднее арифметическое по массиву:
av_max = (average_number * 0.2) + average_number
# Вычисляем число, которое на 20 процентов меньше, чем среднее арифметическое по массиву:
av_min = average_number - (average_number * 0.2)

# Выводим необходимы числа для проверки результатов:
print('Среднее арифметическое:', average_number)
print('Число, которое на 20 процентов больше, чем среднее арифметическое по массиву:', av_max)
print('Число, которое на 20 процентов меньше, чем среднее арифметическое по массиву:', av_min)

# Сравниваем элементы первого массива со значениями av_max и av_min
# Если элемент массива равен av_max или av_min, записываем его во второй массив
for l in my_array:
    if l == av_min or l == av_max:
        my_array2.append(l)

# Вывод итогового массива
if len(my_array2) == 0:
    print('Не удалось получить значения, удовлетворяющие критериям поиска')
else:
    print('Массив из чисел, которые удовлетворяют критерии поиска:', my_array2)