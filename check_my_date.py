from array import *
import sys
import datetime


# Написать функцию check_my_date, принимающую число месяц и год.
# Вернуть True, если такая дата существует и False иначе.
# Объяснить выбор решения. Написать тесты на свой код


def input_values():
    """получаем дату и записываем ее в массив"""

    print('Введите дату в формате yyyy/mm/dd:')

    an_array = array('i')
    for i in range(0, 4):

        if i == 1:
            print('Введите год:')
            an_array.append(int(input()))


        if i == 2:
            print('Введите месяц:')
            an_array.append(int(input()))

        if i == 3:
            print('Введите день:')
            an_array.append(int(input()))

    return an_array


def validator(year, month, day):
        if year < 0:
            print("Вы ввели некорректный год! Попробуете ввести значения еще раз? Y/N")
            choice()

        if month <= 0 or month > 12:
            print("Вы ввели некорректный месяц! Попробуете ввести значения еще раз? Y/N")
            choice()

        if day <= 0 or day > 31:
            print("Вы ввели некорректный день! Попробуете ввести значения еще раз? Y/N")
            choice()


def check_my_date(year, month, day):
    today = datetime.date.today()
    date_to_check = datetime.date(year, month, day)
    answer = True
    if date_to_check > today:
        return False
    else:
        return True


def choice():
    """навигация пользователя после завершения вычислений или ввода невалидных значений"""

    choice = input().lower()
    if choice == 'y':
        main()
    if choice == 'n':
        print('Программа успешно завершена!')
        sys.exit()
    else:
        print('Программа успешно завершена!')
        sys.exit()


def main():
    """точка старта программы"""

    array = input_values()
    validator(array[0], array[1], array[2])
    print(f"Вы ввели дату: {array[0]}-{array[1]}-{array[2]}")
    print('Существует ли такая дата:', check_my_date(array[0], array[1], array[2]))

if __name__ == '__main__':
    main()
