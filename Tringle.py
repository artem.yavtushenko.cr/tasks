from array import *
import random
import math
import sys
import os


class Triangle():
    """Модель треугольника"""

    def __init__(self, first_side, second_side, third_side):
        """инициализируем стороны треугольника"""

        self.first_side = first_side
        self.second_side = second_side
        self.third_side = third_side
        # print("Треугольник создан!")

    def triangle_type(self):
        """Определим тип треугольника"""

        # валидируем стороны треугольника введенные пользователем:
        if (self.first_side <= 0 or
                self.second_side <= 0 or
                self.third_side <= 0):
            print("Треугольник с такими сторонами не может существовать. Попробуете ввести значение сторон снова? Y/N")
            # вызываем функцию навигации по программе
            choise()

        if (self.first_side + self.second_side <= self.third_side or
                self.first_side + self.third_side <= self.second_side or
                self.second_side + self.third_side <= self.first_side):
            print("Треугольник с такими сторонами не может существовать. Попробуете ввести значение сторон снова? Y/N")

            # вызываем функцию навигации по программе
            choise()

        if (self.first_side == self.second_side and self.second_side == self.third_side):
            print('Это равносторонний треугольник')
            tType = 1
            return tType

        if (self.first_side == self.second_side and self.second_side != self.third_side or
                self.first_side == self.third_side and self.third_side != self.second_side or
                self.second_side == self.third_side and self.third_side != self.first_side):
            print("Это равнобедренный треугольник")
            tType = 2
            return tType

        if (self.first_side ** 2 + self.second_side ** 2 == self.third_side ** 2 or
                self.second_side ** 2 + self.third_side ** 2 == self.first_side ** 2 or
                self.first_side ** 2 + self.third_side ** 2 == self.second_side ** 2):
            print("Это прямоугольынй треугольник")
            tType = 4
            return tType

        if (self.first_side != self.second_side and
                self.second_side != self.third_side and
                self.first_side != self.third_side):
            print("Это треугольник, у которого все стороны различаются по длине")
            tType = 3
            return tType


def calculations(trType, a, b, c):
    """вычисляем площаль и периметр треугольника, в зависимости от его типа"""

    # получаем три стороны:
    first_side = a
    second_side = b
    third_side = c

    # вычисление площади и периметра для равностороннего треугольника (trType == 1):
    if trType == 1:
        # формула для вычисления периметра равностороннего треугольника
        P = first_side * 3

        # формула для вычисления площади равностороннего треугольника
        S = (math.sqrt(3) / 4) * first_side ** 2

        # выводим пользователю результат вычислений
        print('Его периметр равен: ', P, '\n', 'Его площадь равна: ', S, sep=(''))
        print('Желаете продолжить работу с программой? Y/N')

        # вызываем функцию навигации по программе
        choise()

    # вычисление площади и периметра для равнобедренного треугольника (trType == 2):
    if trType == 2:
        # формула для вычисления периметра равнобедренного треугольника
        P = first_side + second_side + third_side

        # полупериметр (необходим для формулы Герона)
        p = P / 2

        # формула для вычисления площади равнобедренного треугольника по формуле Герона
        S = math.sqrt(p * (p - first_side) * (p - second_side) * (p - third_side))

        # выводим пользователю результат вычислений
        print('Его периметр равен: ', P, '\n', 'Его площадь равна: ', S, sep=(''))
        print('Желаете продолжить работу с программой? Y/N')

        # вызываем функцию навигации по программе
        choise()

    # вычисление площади и периметра для треугольника с тремя разными сторонами (trType == 3):
    if trType == 3:
        # формула для вычисления периметра для треугольника с тремя разными сторонами
        P = first_side + second_side + third_side

        # полупериметр (необходим для формулы Герона)
        p = P / 2

        # формула для вычисления площади равностороннего треугольника
        S = math.sqrt(p * (p - first_side) * (p - second_side) * (p - third_side))

        # выводим пользователю результат вычислений
        print('Его периметр равен: ', P, '\n', 'Его площадь равна: ', S, sep=(''))
        print('Желаете продолжить работу с программой? Y/N')

        # вызываем функцию навигации по программе
        choise()

    # вычисление площади и периметра для прямоугольного треугольника (trType == 4):
    if trType == 4:
        # формула для вычисления периметра для треугольника с тремя разными сторонами
        P = first_side + second_side + third_side

        # полупериметр (необходим для формулы Герона)
        p = P / 2

        # формула для вычисления площади равностороннего треугольника
        S = math.sqrt(p * (p - first_side) * (p - second_side) * (p - third_side))

        # выводим пользователю результат вычислений
        print('Его периметр равен: ', P, '\n', 'Его площадь равна: ', S, sep=(''))
        print('Желаете продолжить работу с программой? Y/N')

        # вызываем функцию навигации по программе
        choise()


def input_values():
    """получаем три стороны треугольника и запичываем в массив"""

    print('Введите 3 целочисленных значения, чтобы инициализировать стороны треугольника:')

    an_array = array("i")
    for i in range(0, 3):
        an_array.append(int(input()))
    return an_array


def choise():
    """навигация пользователя после завершения вычислений или ввода невалидных значений"""

    choise = input().lower()
    if choise == 'y':
        main()
    if choise == 'n':
        print('Программа успешно завершена!')
        sys.exit()
    else:
        print('Программа успешно завершена!')
        sys.exit()


def main():
    """точка старта программы"""

    # вызываем функцию ввода сторон треугольника и записываем их в массив
    test_array = input_values()

    # передаем значения сторон в конструктор
    test = Triangle(test_array[0], test_array[1], test_array[2])

    # определяем тип треугольника с помощью test.triangle_type() и вызываем функцию для вычисления S, P, передавая ей 3 стороны и тип треугольника
    tr_type = calculations(test.triangle_type(), test_array[0], test_array[1], test_array[2])


if __name__ == '__main__':
    main()

# print(test.triangle_type())



