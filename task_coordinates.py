from array import *
import sys
from geopy.distance import geodesic


# Дано:
#  список координат:
# сoordinates_list = [(48.474095989326635,35.022104566140605),
# (48.45643,35.04729),
# (48.45966456386765,35.05575204083053),
# (48.461848463221855,35.035789475581964)]
# Текущая локация (выбрать самостоятельно)
# Радиус (выбрать самостоятельно)
# Задача:
# Написать  функцию, которая будет принимать текущую локацию(my_location), список координат (coordinates_list),
# Функция должна возвращать отсортированный список координат по удаленности от текущей локации
# Написать  функцию, которая будет принимать твою текущую локацию(my_location), список координат (coordinates_list) и радиус (r).
# Функция должна возвращать отфильтрованный список координат в радиусе R (выбрать самостоятельно) от твоей текущей локации.




def input_current_location():
    """Просим пользователя ввести координаты текущей локации. Функция вернет массив, состоящий из двух точек: широта и долгота"""

    # созлаем массив, в который запишем координаты текущей локации
    current_location = array("f")

    # запишем в массив значеня широты и долготы с клавиатуры
    print('Введите координаты текущей локации:')
    for i in range(0, 3):
        if i == 1:
            print("Широта: ")
            current_location.append(float(input()))
            #добавить валидацию (широта не больше чем 90 должна быть)
            # UPD: geopy валидирует самостоятельно
        if i == 2:
            print("Долгота: ")
            current_location.append(float(input()))

    return current_location

def input_radius():
    """Просим пользователя ввести радиус. Функция вернет радиус"""

    print('Введите радиус (в километрах):')
    radius = float(input())
    return radius


def find_range(current_location_lat, current_location_long, coordinate_list, radius):
    """Считаем расстояние между текущей точкой и точками из условия и сортируем полученный список по расстоянию"""

    #список для записи расстояний между точками
    answer_list = []
    #список - финальный ответ, в который мы приклеиваем значения расстояний к точкам, что их дали
    an_answer = []

    #посчитали расстояние, записали в список и, по окончанию цикла, отсортировали
    for k in coordinate_list:
        answer_list.append(geodesic((current_location_lat, current_location_long), k) . km)
    answer_list.sort()

    #выводим пользователю информацию о расстоянии между указанной точкой и точками в списке с условия, с указанием этих точек
    for p in answer_list:

        print(f'Расстояние от указанной пользователем точки ({current_location_lat}, {current_location_long})'
              f' до точки {coordinate_list[answer_list.index(p)]} cоставляет: {answer_list[answer_list.index(p)]} километров')

    #выводим пользователю информацию о точках со списка - условия и расстояниями от них до указанной точки в указанном радиусе
    for v in answer_list:
        #нас интересуют точки, что попадают в значение радиуса
        if v <= radius:
            an_answer.append(v)
            print(f'В радиусе {radius} от указанной пользователем точки ({current_location_lat}, {current_location_long})'
                    f' находится точка: {coordinate_list[an_answer.index(v)]} на расстоянии {answer_list[answer_list.index(v)]} километров')

    print(f'В радиусе {radius} от указанной пользователем точки ({current_location_lat}, {current_location_long}) не находится ни одна точка из списка - условия')


def validatorr(current_location_lat, current_location_long):

        if (current_location_lat < -90 or current_location_lat > 90) and (current_location_long < -180 or current_location_long > 180):
            print("Вы ввели неверную широту и долготу! Попробуете снова? Y/N")
            choice()

        elif current_location_lat < -90 or current_location_lat > 90:
            print("Вы ввели неверную широту! Попробуете снова? Y/N")
            choice()
        elif current_location_long < -180 or current_location_long > 180:
            print("Вы ввели неверную долготу! Попробуете снова? Y/N")
            choice()


def choice():
    """навигация пользователя после завершения вычислений или ввода невалидных значений"""

    print('Ответ:')
    a_letter: str = input().lower()
    print(a_letter)
    if a_letter == 'y':
        main()
    if a_letter == 'n':
        print('Программа успешно завершена!')
        sys.exit()
    else:
        print('Программа успешно завершена!')
        sys.exit()


def main():
    """точка старта программы"""

    coordinates_list = [(48.474095989326635, 35.022104566140605), (48.45643, 35.04729),
                    (48.45966456386765, 35.05575204083053), (48.461848463221855, 35.035789475581964)]

    current_location = input_current_location()
    validatorr(current_location[0], current_location[1])
    radius = input_radius()

    print('Вывожу отсортированный результат: ')
    find_range(current_location[0], current_location[1], coordinates_list, radius)

if __name__ == '__main__':
    main()
